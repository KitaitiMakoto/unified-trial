import h from 'virtual-dom/h';
import createElement from 'virtual-dom/create-element';
import diff from 'virtual-dom/diff';
import patch from 'virtual-dom/patch';
import unified from 'unified';
import english from 'retext-english';
import visit from 'unist-util-visit';

var hues = [
  60,
  60,
  60,
  300,
  300,
  0,
  0,
  120,
  120,
  120,
  120,
  120,
  120,
  180
];

var processor = unified().use(english);
var root = document.getElementById('root');
var tree = render('The initial text.');
var dom = root.appendChild(createElement(tree));

setTimeout(resize, 4);

function onchange(event) {
  let next = render(event.target.value);
  dom = patch(dom, diff(tree, next));
  tree = next;
  setTimeout(resize, 4);
}

function render(text) {
  let node = parse(text);
  let key = 0;

  return h('div', {className: 'editor'}, [
    h('div', {key: 'draw', className: 'draw'}, highlight(node)),
    h('textarea', {
      key: 'area',
      value: text,
      oninput: onchange
    })
  ]);

  function parse(value) {
    return processor.runSync(processor.parse(value));
  }

  function highlight(node) {
    let children = node.children;
    let length = children.length;
    let index = -1;
    let results = [];

    while (++index < length) {
      results = results.concat(one(children[index]));
    }

    return results;
  }

  function one(node) {
    let result = 'value' in node ? node.value : highlight(node);

    if (node.type === 'SentenceNode') {
      key++;
      result = h('span', {
        key: `s-${key}`,
        style: {backgroundColor: color(count(node))}
      }, result);
    }

    return result;

    function count(node) {
      let value = 0;

      visit(node, 'WordNode', add);

      return value;

      function add() {
        value++;
      }
    }

    function color(count) {
      let val = count < hues.length ? hues[count] : hues[hues.length - 1];
      return `hsl(${[val, '93%', '85%'].join(', ')})`;
    }
  }
}

function resize() {
  console.log(dom.firstChild);
  dom.lastChild.rows = Math.ceil(
    dom.firstChild.getBoundingClientRect().height /
      parseInt(window.getComputedStyle(dom.firstChild).lineHeight, 10)
  ) + 1;
}
