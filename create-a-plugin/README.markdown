Create a plugin
===============

Code snippets of [Create a plugin][] guide.

[Create a plugin]: https://unifiedjs.github.io/create-a-plugin.html
