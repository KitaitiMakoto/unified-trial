const fs = require('fs');
const retext = require('retext');
const report = require('vfile-reporter');
const spacing = require('./index.js');

var doc = fs.readFileSync('example.md');

retext()
  .use(spacing)
  .process(doc, (err, file) => {
    console.error(report(err || file));
  });
